var $$ = Dom7;

var app = new Framework7({
  root: '#app', // App root element
  name: 'ToDo App', // App name
  theme: 'auto', // Automatic theme detection
  panel: {
    swipe: 'left'
  },
  theme: 'auto',
  // App root data
  data: function () {
    return {
      loading: true,
      effect: 'fade' // 'fade/blink/pulse' 
    };
  },
  view: {
    pushState: false
  },
  // App root methods
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
  },
  // App routes
  routes: routes,
});

Template7.registerHelper('equal', function(param1, param2) {
  return param1 === param2;
});

// Init/Create main view
var mainView = app.views.create('.view-main', {
    componentUrl: './index.html'
});

// app.preloader.show('multi');

var $ptrContent = $$('.ptr-content');
$ptrContent.on('ptr:refresh', function (e) {
    console.log(e)
    setTimeout(function () {
        app.ptr.done();
    }, 2000);
});

// app.on('pageBeforeIn', function (e, page) {
//   // app.data.loading = true;
//   $$('.page_wrap').hide();
//   $$('.skeleton').show();
// });

// app.on('pageAfterIn', function (e, page) {
//   // app.data.loading = false;
//   $$('.page_wrap').show();
//   $$('.skeleton').hide();
// });


// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  // Close login screen
  app.loginScreen.close('#my-login-screen');

  // Alert username and password
  app.dialog.alert('Username: ' + username + '<br>Password: ' + password);
});

// Form Submit
app.on('formAjaxBeforeSend', function (formEl, data, xhr) {
  app.preloader.show();
});

app.on('formAjaxSuccess', function (formEl, data, xhr) {
  var callback = JSON.parse(xhr.response);
  app.toast.create({text: callback.res, closeButton: true}).open();
});

app.on('formAjaxComplete', function (formEl, data, xhr) {
  app.preloader.hide();
  var callback = JSON.parse(xhr.response);
  app.views.main.router.navigate(callback.redirect);
});

// $$('form.form-ajax-submit').on('formajax:success', function(e){
//   var xhr = e.detail.xhr;
//   var data = e.detail.data;
//   console.log(data);
// });

function destroy(url)
{
  app.request.get(url, function (data) {
    app.views.main.router.navigate(data.redirect, {reloadCurrent: true});
    app.toast.create({text: data.res, closeButton: true,}).open();
  }, function (errorRes) {}, 'json');
}