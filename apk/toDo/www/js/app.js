var $$ = Dom7;

var app = new Framework7({
  root: '#app', // App root element
  name: 'ToDo App', // App name
  theme: 'auto', // Automatic theme detection
  // App root data
  data: function () {
    return {
      user: {
        firstName: 'John',
        lastName: 'Doe',
      },

    };
  },
  // App root methods
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
  },
  // App routes
  routes: routes,
});

Template7.registerHelper('equal', function(param1, param2) {
  return param1 === param2;
});

// app.preloader.show('multi');

// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  // Close login screen
  app.loginScreen.close('#my-login-screen');

  // Alert username and password
  app.dialog.alert('Username: ' + username + '<br>Password: ' + password);
});

// Form Submit
app.on('formAjaxBeforeSend', function (formEl, data, xhr) {
  app.preloader.show();
});

app.on('formAjaxSuccess', function (formEl, data, xhr) {
  var callback = JSON.parse(xhr.response);
  app.toast.create({text: callback.res, closeButton: true}).open();
});

app.on('formAjaxComplete', function (formEl, data, xhr) {
  app.preloader.hide();
  var callback = JSON.parse(xhr.response);
  app.views.main.router.navigate(callback.redirect);
});

// $$('form.form-ajax-submit').on('formajax:success', function(e){
//   var xhr = e.detail.xhr;
//   var data = e.detail.data;
//   console.log(data);
// });

function destroy(url)
{
  app.request.get(url, function (data) {
    app.views.main.router.navigate(data.redirect, {reloadCurrent: true});
    app.toast.create({text: data.res, closeButton: true,}).open();
  }, function (errorRes) {}, 'json');
}