<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        header('content-type: application/json; charset=utf-8');
    }

    public function index()
    {
        $task_list = $this->My_crud->get_all_row('task','','id ASC','');
        echo json_encode($task_list, JSON_UNESCAPED_SLASHES);
    }

    public function create()
    {
        
    }

    public function show()
    {
        
    }

    public function store()
    {
        $data['task_name']      = $this->input->post('task_name');
        $data['date']           = $this->input->post('date');
        $data['start_time']     = $this->input->post('start_time');
        $data['end_time']       = $this->input->post('end_time');
        $data['status']         = $this->input->post('status');
        $data['created_at']     = date('Y-m-d H:m:s');

        $this->db->insert('task', $data);
        echo json_encode(['type' => 'success', 'res' => 'Task Create Successfull', 'redirect' => '/task/list/'], JSON_UNESCAPED_SLASHES);
    }

    public function edit($id)
    {
         $task_list = $this->My_crud->get_one_row('task',['id' => $id],'id ASC','');
        echo json_encode($task_list, JSON_UNESCAPED_SLASHES);
    }

    public function update($id)
    {
        $data['task_name']      = $this->input->post('task_name');
        $data['date']           = $this->input->post('date');
        $data['start_time']     = $this->input->post('start_time');
        $data['end_time']       = $this->input->post('end_time');
        $data['status']         = $this->input->post('status');
        $data['created_at']     = date('Y-m-d H:m:s');

        $this->db->update('task', $data, ['id' => $id]);
        echo json_encode(['type' => 'success', 'res' => 'Task Update Successfull', 'redirect' => '/task/list/'], JSON_UNESCAPED_SLASHES);
    }

    public function destroy($id)
    {
         $this->db->delete('task', ['id' => $id]);
         echo json_encode(['type' => 'success', 'res' => 'Task Delete Successfull', 'redirect' => '/task/list/'], JSON_UNESCAPED_SLASHES);
    }
}

?>
